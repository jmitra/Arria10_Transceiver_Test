set counter 100

set issp_index 0
set issp [lindex [get_service_paths issp] 0]
set claimed_issp [claim_service issp $issp mylib]
array set instance_info [issp_get_instance_info $claimed_issp]
set source_width $instance_info(source_width)
set probe_width $instance_info(probe_width)

# Reset phase calc component    

set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data | 0x020 }]
issp_write_source_data $claimed_issp $modified_source_data

set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data | 0x001 }]
issp_write_source_data $claimed_issp $modified_source_data

set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data & 0xFFE }]
issp_write_source_data $claimed_issp $modified_source_data

after 1000
while {$counter} {
set counter [expr $counter - 1] 


proc get_probe_line_data {all_probe_data index} {
set line_data [expr { ($all_probe_data >> $index) & 1 }]
return $line_data
}
    proc hex2dec {largeHex} {
        set res 0
        foreach hexDigit [split $largeHex {}] {
            set new 0x$hexDigit
            set res [expr {16*$res + $new}]
        }
        return $res
    }
    
# Reset phase calc component    
set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data & 0x1FF }]
issp_write_source_data $claimed_issp $modified_source_data

set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data | 0x200 }]
issp_write_source_data $claimed_issp $modified_source_data

set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data & 0x1FF }]
issp_write_source_data $claimed_issp $modified_source_data

set phase_value_calc_done "0"

# set phase_value           [hex2dec $phase_value_hex]
while {$phase_value_calc_done != "1"} {
set all_probe_data [issp_read_probe_data $claimed_issp]
set phase_value_calc_done [get_probe_line_data $all_probe_data 145]
set phase_value_sign      [get_probe_line_data $all_probe_data 144]

set phase_value           [expr ($all_probe_data >> 112) & 0xffffffff]
if {$phase_value_calc_done == "1"} {
    if {$phase_value_sign == "0"} {puts "$phase_value"} \
        else {
            puts "[expr (-$phase_value)]"
        }
    }
}

# Reset Rx Clk Slip component    
set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data & 0x3EF }]
issp_write_source_data $claimed_issp $modified_source_data

set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data | 0x010 }]
issp_write_source_data $claimed_issp $modified_source_data

set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data & 0x3EF }]
issp_write_source_data $claimed_issp $modified_source_data

set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data | 0x010 }]
issp_write_source_data $claimed_issp $modified_source_data

set current_source_data [issp_read_source_data $claimed_issp]
set modified_source_data [expr { $current_source_data & 0x3EF }]
issp_write_source_data $claimed_issp $modified_source_data

}

close_service issp $claimed_issp