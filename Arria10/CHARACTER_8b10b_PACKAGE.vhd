--=================================================================================================--
--##################################   Package Information   ######################################--
--=================================================================================================--
-------------------------------------------------------------------------------
-- Title      : PHYSICAL LAYER CONNECTIONS 
-- Project    : TTC-PON 2015
-------------------------------------------------------------------------------
-- File       : CHARACTER_8b_10b_PACKAGE.vhd
-- Author     : Jubin MITRA (jmitra@cern.ch)
-- Company    : VECC
-- Created    : 15-03-2016
-- Last update: 15-03-2016
-- Platform   : 
-- Standard   : VHDL'93/08                  
-- Revision   : 1.0                                                     
-- Target Device:         Altera Arria 10                                                          
-- Tool version:          Quartus II 15.1                                                                
-------------------------------------------------------------------------------
-- Description: 
-- This package contains constant defined for 8b10b data and control groups								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author          Description
-- 15-03-2016  1.0      Jubin Mitra	    Created
-------------------------------------------------------------------------------
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--=================================================================================================--
--##################################   Package Declaration   ######################################--
--=================================================================================================--

package CHARACTER_8b_10b_PACKAGE is
   
   --=================================== CONSTANT DECLARATIONS ==================================--

   --==============================================================================================--
   --====================================== DATA CHARACTERS =======================================--
   --==============================================================================================--   
   
   
   constant D0_0                            : std_logic_vector(7 downto 0) := x"00";
   constant D1_0                            : std_logic_vector(7 downto 0) := x"01";
   constant D2_0                            : std_logic_vector(7 downto 0) := x"02";
   constant D3_0                            : std_logic_vector(7 downto 0) := x"03";
   constant D4_0                            : std_logic_vector(7 downto 0) := x"04";
   constant D5_0                            : std_logic_vector(7 downto 0) := x"05";
   constant D6_0                            : std_logic_vector(7 downto 0) := x"06";
   constant D7_0                            : std_logic_vector(7 downto 0) := x"07";
   constant D8_0                            : std_logic_vector(7 downto 0) := x"08";
   constant D9_0                            : std_logic_vector(7 downto 0) := x"09";
   constant D10_0                            : std_logic_vector(7 downto 0) := x"0A";
   constant D11_0                            : std_logic_vector(7 downto 0) := x"0B";
   constant D12_0                            : std_logic_vector(7 downto 0) := x"0C";
   constant D13_0                            : std_logic_vector(7 downto 0) := x"0D";
   constant D14_0                            : std_logic_vector(7 downto 0) := x"0E";
   constant D15_0                            : std_logic_vector(7 downto 0) := x"0F";
   constant D16_0                            : std_logic_vector(7 downto 0) := x"10";
   constant D17_0                            : std_logic_vector(7 downto 0) := x"11";
   constant D18_0                            : std_logic_vector(7 downto 0) := x"12";
   constant D19_0                            : std_logic_vector(7 downto 0) := x"13";
   constant D20_0                            : std_logic_vector(7 downto 0) := x"14";
   constant D21_0                            : std_logic_vector(7 downto 0) := x"15";
   constant D22_0                            : std_logic_vector(7 downto 0) := x"16";
   constant D23_0                            : std_logic_vector(7 downto 0) := x"17";
   constant D24_0                            : std_logic_vector(7 downto 0) := x"18";
   constant D25_0                            : std_logic_vector(7 downto 0) := x"19";
   constant D26_0                            : std_logic_vector(7 downto 0) := x"1A";
   constant D27_0                            : std_logic_vector(7 downto 0) := x"1B";
   constant D28_0                            : std_logic_vector(7 downto 0) := x"1C";
   constant D29_0                            : std_logic_vector(7 downto 0) := x"1D";
   constant D30_0                            : std_logic_vector(7 downto 0) := x"1E";
   constant D31_0                            : std_logic_vector(7 downto 0) := x"1F";
   constant D0_1                            : std_logic_vector(7 downto 0) := x"20";
   constant D1_1                            : std_logic_vector(7 downto 0) := x"21";
   constant D2_1                            : std_logic_vector(7 downto 0) := x"22";
   constant D3_1                            : std_logic_vector(7 downto 0) := x"23";
   constant D4_1                            : std_logic_vector(7 downto 0) := x"24";
   constant D5_1                            : std_logic_vector(7 downto 0) := x"25";
   constant D6_1                            : std_logic_vector(7 downto 0) := x"26";
   constant D7_1                            : std_logic_vector(7 downto 0) := x"27";
   constant D8_1                            : std_logic_vector(7 downto 0) := x"28";
   constant D9_1                            : std_logic_vector(7 downto 0) := x"29";
   constant D10_1                            : std_logic_vector(7 downto 0) := x"2A";
   constant D11_1                            : std_logic_vector(7 downto 0) := x"2B";
   constant D12_1                            : std_logic_vector(7 downto 0) := x"2C";
   constant D13_1                            : std_logic_vector(7 downto 0) := x"2D";
   constant D14_1                            : std_logic_vector(7 downto 0) := x"2E";
   constant D15_1                            : std_logic_vector(7 downto 0) := x"2F";
   constant D16_1                            : std_logic_vector(7 downto 0) := x"30";
   constant D17_1                            : std_logic_vector(7 downto 0) := x"31";
   constant D18_1                            : std_logic_vector(7 downto 0) := x"32";
   constant D19_1                            : std_logic_vector(7 downto 0) := x"33";
   constant D20_1                            : std_logic_vector(7 downto 0) := x"34";
   constant D21_1                            : std_logic_vector(7 downto 0) := x"35";
   constant D22_1                            : std_logic_vector(7 downto 0) := x"36";
   constant D23_1                            : std_logic_vector(7 downto 0) := x"37";
   constant D24_1                            : std_logic_vector(7 downto 0) := x"38";
   constant D25_1                            : std_logic_vector(7 downto 0) := x"39";
   constant D26_1                            : std_logic_vector(7 downto 0) := x"3A";
   constant D27_1                            : std_logic_vector(7 downto 0) := x"3B";
   constant D28_1                            : std_logic_vector(7 downto 0) := x"3C";
   constant D29_1                            : std_logic_vector(7 downto 0) := x"3D";
   constant D30_1                            : std_logic_vector(7 downto 0) := x"3E";
   constant D31_1                            : std_logic_vector(7 downto 0) := x"3F";
   constant D0_2                            : std_logic_vector(7 downto 0) := x"40";
   constant D1_2                            : std_logic_vector(7 downto 0) := x"41";
   constant D2_2                            : std_logic_vector(7 downto 0) := x"42";
   constant D3_2                            : std_logic_vector(7 downto 0) := x"43";
   constant D4_2                            : std_logic_vector(7 downto 0) := x"44";
   constant D5_2                            : std_logic_vector(7 downto 0) := x"45";
   constant D6_2                            : std_logic_vector(7 downto 0) := x"46";
   constant D7_2                            : std_logic_vector(7 downto 0) := x"47";
   constant D8_2                            : std_logic_vector(7 downto 0) := x"48";
   constant D9_2                            : std_logic_vector(7 downto 0) := x"49";
   constant D10_2                            : std_logic_vector(7 downto 0) := x"4A";
   constant D11_2                            : std_logic_vector(7 downto 0) := x"4B";
   constant D12_2                            : std_logic_vector(7 downto 0) := x"4C";
   constant D13_2                            : std_logic_vector(7 downto 0) := x"4D";
   constant D14_2                            : std_logic_vector(7 downto 0) := x"4E";
   constant D15_2                            : std_logic_vector(7 downto 0) := x"4F";
   constant D16_2                            : std_logic_vector(7 downto 0) := x"50";
   constant D17_2                            : std_logic_vector(7 downto 0) := x"51";
   constant D18_2                            : std_logic_vector(7 downto 0) := x"52";
   constant D19_2                            : std_logic_vector(7 downto 0) := x"53";
   constant D20_2                            : std_logic_vector(7 downto 0) := x"54";
   constant D21_2                            : std_logic_vector(7 downto 0) := x"55";
   constant D22_2                            : std_logic_vector(7 downto 0) := x"56";
   constant D23_2                            : std_logic_vector(7 downto 0) := x"57";
   constant D24_2                            : std_logic_vector(7 downto 0) := x"58";
   constant D25_2                            : std_logic_vector(7 downto 0) := x"59";
   constant D26_2                            : std_logic_vector(7 downto 0) := x"5A";
   constant D27_2                            : std_logic_vector(7 downto 0) := x"5B";
   constant D28_2                            : std_logic_vector(7 downto 0) := x"5C";
   constant D29_2                            : std_logic_vector(7 downto 0) := x"5D";
   constant D30_2                            : std_logic_vector(7 downto 0) := x"5E";
   constant D31_2                            : std_logic_vector(7 downto 0) := x"5F";
   constant D0_3                            : std_logic_vector(7 downto 0) := x"60";
   constant D1_3                            : std_logic_vector(7 downto 0) := x"61";
   constant D2_3                            : std_logic_vector(7 downto 0) := x"62";
   constant D3_3                            : std_logic_vector(7 downto 0) := x"63";
   constant D4_3                            : std_logic_vector(7 downto 0) := x"64";
   constant D5_3                            : std_logic_vector(7 downto 0) := x"65";
   constant D6_3                            : std_logic_vector(7 downto 0) := x"66";
   constant D7_3                            : std_logic_vector(7 downto 0) := x"67";
   constant D8_3                            : std_logic_vector(7 downto 0) := x"68";
   constant D9_3                            : std_logic_vector(7 downto 0) := x"69";
   constant D10_3                            : std_logic_vector(7 downto 0) := x"6A";
   constant D11_3                            : std_logic_vector(7 downto 0) := x"6B";
   constant D12_3                            : std_logic_vector(7 downto 0) := x"6C";
   constant D13_3                            : std_logic_vector(7 downto 0) := x"6D";
   constant D14_3                            : std_logic_vector(7 downto 0) := x"6E";
   constant D15_3                            : std_logic_vector(7 downto 0) := x"6F";
   constant D16_3                            : std_logic_vector(7 downto 0) := x"70";
   constant D17_3                            : std_logic_vector(7 downto 0) := x"71";
   constant D18_3                            : std_logic_vector(7 downto 0) := x"72";
   constant D19_3                            : std_logic_vector(7 downto 0) := x"73";
   constant D20_3                            : std_logic_vector(7 downto 0) := x"74";
   constant D21_3                            : std_logic_vector(7 downto 0) := x"75";
   constant D22_3                            : std_logic_vector(7 downto 0) := x"76";
   constant D23_3                            : std_logic_vector(7 downto 0) := x"77";
   constant D24_3                            : std_logic_vector(7 downto 0) := x"78";
   constant D25_3                            : std_logic_vector(7 downto 0) := x"79";
   constant D26_3                            : std_logic_vector(7 downto 0) := x"7A";
   constant D27_3                            : std_logic_vector(7 downto 0) := x"7B";
   constant D28_3                            : std_logic_vector(7 downto 0) := x"7C";
   constant D29_3                            : std_logic_vector(7 downto 0) := x"7D";
   constant D30_3                            : std_logic_vector(7 downto 0) := x"7E";
   constant D31_3                            : std_logic_vector(7 downto 0) := x"7F";
   constant D0_4                            : std_logic_vector(7 downto 0) := x"80";
   constant D1_4                            : std_logic_vector(7 downto 0) := x"81";
   constant D2_4                            : std_logic_vector(7 downto 0) := x"82";
   constant D3_4                            : std_logic_vector(7 downto 0) := x"83";
   constant D4_4                            : std_logic_vector(7 downto 0) := x"84";
   constant D5_4                            : std_logic_vector(7 downto 0) := x"85";
   constant D6_4                            : std_logic_vector(7 downto 0) := x"86";
   constant D7_4                            : std_logic_vector(7 downto 0) := x"87";
   constant D8_4                            : std_logic_vector(7 downto 0) := x"88";
   constant D9_4                            : std_logic_vector(7 downto 0) := x"89";
   constant D10_4                            : std_logic_vector(7 downto 0) := x"8A";
   constant D11_4                            : std_logic_vector(7 downto 0) := x"8B";
   constant D12_4                            : std_logic_vector(7 downto 0) := x"8C";
   constant D13_4                            : std_logic_vector(7 downto 0) := x"8D";
   constant D14_4                            : std_logic_vector(7 downto 0) := x"8E";
   constant D15_4                            : std_logic_vector(7 downto 0) := x"8F";
   constant D16_4                            : std_logic_vector(7 downto 0) := x"90";
   constant D17_4                            : std_logic_vector(7 downto 0) := x"91";
   constant D18_4                            : std_logic_vector(7 downto 0) := x"92";
   constant D19_4                            : std_logic_vector(7 downto 0) := x"93";
   constant D20_4                            : std_logic_vector(7 downto 0) := x"94";
   constant D21_4                            : std_logic_vector(7 downto 0) := x"95";
   constant D22_4                            : std_logic_vector(7 downto 0) := x"96";
   constant D23_4                            : std_logic_vector(7 downto 0) := x"97";
   constant D24_4                            : std_logic_vector(7 downto 0) := x"98";
   constant D25_4                            : std_logic_vector(7 downto 0) := x"99";
   constant D26_4                            : std_logic_vector(7 downto 0) := x"9A";
   constant D27_4                            : std_logic_vector(7 downto 0) := x"9B";
   constant D28_4                            : std_logic_vector(7 downto 0) := x"9C";
   constant D29_4                            : std_logic_vector(7 downto 0) := x"9D";
   constant D30_4                            : std_logic_vector(7 downto 0) := x"9E";
   constant D31_4                            : std_logic_vector(7 downto 0) := x"9F";
   constant D0_5                            : std_logic_vector(7 downto 0) := x"A0";
   constant D1_5                            : std_logic_vector(7 downto 0) := x"A1";
   constant D2_5                            : std_logic_vector(7 downto 0) := x"A2";
   constant D3_5                            : std_logic_vector(7 downto 0) := x"A3";
   constant D4_5                            : std_logic_vector(7 downto 0) := x"A4";
   constant D5_5                            : std_logic_vector(7 downto 0) := x"A5";
   constant D6_5                            : std_logic_vector(7 downto 0) := x"A6";
   constant D7_5                            : std_logic_vector(7 downto 0) := x"A7";
   constant D8_5                            : std_logic_vector(7 downto 0) := x"A8";
   constant D9_5                            : std_logic_vector(7 downto 0) := x"A9";
   constant D10_5                            : std_logic_vector(7 downto 0) := x"AA";
   constant D11_5                            : std_logic_vector(7 downto 0) := x"AB";
   constant D12_5                            : std_logic_vector(7 downto 0) := x"AC";
   constant D13_5                            : std_logic_vector(7 downto 0) := x"AD";
   constant D14_5                            : std_logic_vector(7 downto 0) := x"AE";
   constant D15_5                            : std_logic_vector(7 downto 0) := x"AF";
   constant D16_5                            : std_logic_vector(7 downto 0) := x"B0";
   constant D17_5                            : std_logic_vector(7 downto 0) := x"B1";
   constant D18_5                            : std_logic_vector(7 downto 0) := x"B2";
   constant D19_5                            : std_logic_vector(7 downto 0) := x"B3";
   constant D20_5                            : std_logic_vector(7 downto 0) := x"B4";
   constant D21_5                            : std_logic_vector(7 downto 0) := x"B5";
   constant D22_5                            : std_logic_vector(7 downto 0) := x"B6";
   constant D23_5                            : std_logic_vector(7 downto 0) := x"B7";
   constant D24_5                            : std_logic_vector(7 downto 0) := x"B8";
   constant D25_5                            : std_logic_vector(7 downto 0) := x"B9";
   constant D26_5                            : std_logic_vector(7 downto 0) := x"BA";
   constant D27_5                            : std_logic_vector(7 downto 0) := x"BB";
   constant D28_5                            : std_logic_vector(7 downto 0) := x"BC";
   constant D29_5                            : std_logic_vector(7 downto 0) := x"BD";
   constant D30_5                            : std_logic_vector(7 downto 0) := x"BE";
   constant D31_5                            : std_logic_vector(7 downto 0) := x"BF";
   constant D0_6                            : std_logic_vector(7 downto 0) := x"C0";
   constant D1_6                            : std_logic_vector(7 downto 0) := x"C1";
   constant D2_6                            : std_logic_vector(7 downto 0) := x"C2";
   constant D3_6                            : std_logic_vector(7 downto 0) := x"C3";
   constant D4_6                            : std_logic_vector(7 downto 0) := x"C4";
   constant D5_6                            : std_logic_vector(7 downto 0) := x"C5";
   constant D6_6                            : std_logic_vector(7 downto 0) := x"C6";
   constant D7_6                            : std_logic_vector(7 downto 0) := x"C7";
   constant D8_6                            : std_logic_vector(7 downto 0) := x"C8";
   constant D9_6                            : std_logic_vector(7 downto 0) := x"C9";
   constant D10_6                            : std_logic_vector(7 downto 0) := x"CA";
   constant D11_6                            : std_logic_vector(7 downto 0) := x"CB";
   constant D12_6                            : std_logic_vector(7 downto 0) := x"CC";
   constant D13_6                            : std_logic_vector(7 downto 0) := x"CD";
   constant D14_6                            : std_logic_vector(7 downto 0) := x"CE";
   constant D15_6                            : std_logic_vector(7 downto 0) := x"CF";
   constant D16_6                            : std_logic_vector(7 downto 0) := x"D0";
   constant D17_6                            : std_logic_vector(7 downto 0) := x"D1";
   constant D18_6                            : std_logic_vector(7 downto 0) := x"D2";
   constant D19_6                            : std_logic_vector(7 downto 0) := x"D3";
   constant D20_6                            : std_logic_vector(7 downto 0) := x"D4";
   constant D21_6                            : std_logic_vector(7 downto 0) := x"D5";
   constant D22_6                            : std_logic_vector(7 downto 0) := x"D6";
   constant D23_6                            : std_logic_vector(7 downto 0) := x"D7";
   constant D24_6                            : std_logic_vector(7 downto 0) := x"D8";
   constant D25_6                            : std_logic_vector(7 downto 0) := x"D9";
   constant D26_6                            : std_logic_vector(7 downto 0) := x"DA";
   constant D27_6                            : std_logic_vector(7 downto 0) := x"DB";
   constant D28_6                            : std_logic_vector(7 downto 0) := x"DC";
   constant D29_6                            : std_logic_vector(7 downto 0) := x"DD";
   constant D30_6                            : std_logic_vector(7 downto 0) := x"DE";
   constant D31_6                            : std_logic_vector(7 downto 0) := x"DF";
   constant D0_7                            : std_logic_vector(7 downto 0) := x"E0";
   constant D1_7                            : std_logic_vector(7 downto 0) := x"E1";
   constant D2_7                            : std_logic_vector(7 downto 0) := x"E2";
   constant D3_7                            : std_logic_vector(7 downto 0) := x"E3";
   constant D4_7                            : std_logic_vector(7 downto 0) := x"E4";
   constant D5_7                            : std_logic_vector(7 downto 0) := x"E5";
   constant D6_7                            : std_logic_vector(7 downto 0) := x"E6";
   constant D7_7                            : std_logic_vector(7 downto 0) := x"E7";
   constant D8_7                            : std_logic_vector(7 downto 0) := x"E8";
   constant D9_7                            : std_logic_vector(7 downto 0) := x"E9";
   constant D10_7                            : std_logic_vector(7 downto 0) := x"EA";
   constant D11_7                            : std_logic_vector(7 downto 0) := x"EB";
   constant D12_7                            : std_logic_vector(7 downto 0) := x"EC";
   constant D13_7                            : std_logic_vector(7 downto 0) := x"ED";
   constant D14_7                            : std_logic_vector(7 downto 0) := x"EE";
   constant D15_7                            : std_logic_vector(7 downto 0) := x"EF";
   constant D16_7                            : std_logic_vector(7 downto 0) := x"F0";
   constant D17_7                            : std_logic_vector(7 downto 0) := x"F1";
   constant D18_7                            : std_logic_vector(7 downto 0) := x"F2";
   constant D19_7                            : std_logic_vector(7 downto 0) := x"F3";
   constant D20_7                            : std_logic_vector(7 downto 0) := x"F4";
   constant D21_7                            : std_logic_vector(7 downto 0) := x"F5";
   constant D22_7                            : std_logic_vector(7 downto 0) := x"F6";
   constant D23_7                            : std_logic_vector(7 downto 0) := x"F7";
   constant D24_7                            : std_logic_vector(7 downto 0) := x"F8";
   constant D25_7                            : std_logic_vector(7 downto 0) := x"F9";
   constant D26_7                            : std_logic_vector(7 downto 0) := x"FA";
   constant D27_7                            : std_logic_vector(7 downto 0) := x"FB";
   constant D28_7                            : std_logic_vector(7 downto 0) := x"FC";
   constant D29_7                            : std_logic_vector(7 downto 0) := x"FD";
   constant D30_7                            : std_logic_vector(7 downto 0) := x"FE";
   constant D31_7                            : std_logic_vector(7 downto 0) := x"FF";

   --=====================================================================================--
   
   --==============================================================================================--
   --==================================== CONTROL CHARACTERS ======================================--
   --==============================================================================================--   
   constant K28_0                            : std_logic_vector(7 downto 0) := x"1C";
   constant K28_1                            : std_logic_vector(7 downto 0) := x"3C";
   constant K28_2                            : std_logic_vector(7 downto 0) := x"5C";
   constant K28_3                            : std_logic_vector(7 downto 0) := x"7C";
   constant K28_4                            : std_logic_vector(7 downto 0) := x"9C";
   constant K28_5                            : std_logic_vector(7 downto 0) := x"BC";
   constant K28_6                            : std_logic_vector(7 downto 0) := x"DC";
   constant K28_7                            : std_logic_vector(7 downto 0) := x"FC";
 
   --=====================================================================================--   
end CHARACTER_8b_10b_PACKAGE;   
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--