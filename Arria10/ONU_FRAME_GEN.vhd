--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
-------------------------------------------------------------------------------
-- Title      : ONU FRAME GENERATOR 
-- Project    : TTC-PON 2015
-------------------------------------------------------------------------------
-- File       : ONU_FRAME_GEN.vhd
-- Author     : Jubin MITRA (jmitra@cern.ch)
-- Company    : VECC
-- Created    : 15-03-2016
-- Last update: 15-03-2016
-- Platform   : 
-- Standard   : VHDL'93/08                  
-- Revision   : 1.0                                                     
-- Target Device:         Altera Arria 10                                                          
-- Tool version:          Quartus II 15.1                                                                
-------------------------------------------------------------------------------
-- Description: 
-- It wraps the data packet in ONU Frame								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 16-03-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

--! Specific packages
use work.CHARACTER_8b_10b_PACKAGE.all;


--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity ONU_FRAME_GEN is
    port (
        CLK_I             : in  std_logic;  
        RESET_I           : in  std_logic;  

        -- CALIBRATION ENABLE SIGNALS
        CAL_ENA_I         : in  std_logic;   
        FAST_BEAT_I       : in  std_logic;
        SLOW_BEAT_I       : in  std_logic;
        
        -- STANDARD FRAME TX ENABLE SIGNAL
        FRAME_ENA_I       : in  std_logic;
        
        -- INPUT DATA WITH ONU ADDRESS
        TXDATA_I          : in  std_logic_vector(31 downto 0);
        ADDRESS_I         : in  std_logic_vector(7 downto 0);
        READY_O           : out std_logic;
        
        -- 8b10b ENCODED DATA
        TXDATA_O          : out std_logic_vector(31 downto 0);
        TX8B10BBYPASS_O   : out std_logic_vector(3 downto 0);
        TXCHARISK_O       : out std_logic_vector(3 downto 0)
        
        );
end ONU_FRAME_GEN;


--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture mixed of ONU_FRAME_GEN is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   
    type t_frame_ctrl_states is (TX_IDLE, TX_PREAMBLE, TX_START_OF_FRAME, TX_PAYLOAD, TX_GAP);
    signal frame_ctrl_fsm_state                     : t_frame_ctrl_states; 

    ------------------------------------    FOR COMPONENTS   ----------------------------------------
    signal prbs_en          : std_logic;
    signal prbs_dout        : std_logic_vector(15 downto 0);
    signal tx_k28_1         : std_logic;

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   READY_O                                                <= '1' when frame_ctrl_fsm_state = TX_PAYLOAD
                                                        else '0';
   --==============================================================================================--
   --=================================== FRAME CONTROL FSM  =======================================--
   --==============================================================================================--   

    FRAME_CTRL_proc: 
           process(CLK_I)
            begin
                if RESET_I = '1' then
                    frame_ctrl_fsm_state                    <=  TX_IDLE;
                
                elsif rising_edge(CLK_I) then
                    
                        case frame_ctrl_fsm_state is
                            when TX_IDLE =>
                                if CAL_ENA_I = '1' and FAST_BEAT_I = '1' then
                                    frame_ctrl_fsm_state <= TX_PREAMBLE;
                                elsif CAL_ENA_I = '0' and FRAME_ENA_I = '1' then
                                    frame_ctrl_fsm_state <= TX_PREAMBLE;
                                end if; 
                            
                            when TX_PREAMBLE =>
                                frame_ctrl_fsm_state <= TX_START_OF_FRAME;
                                
                            when TX_START_OF_FRAME =>
                                frame_ctrl_fsm_state <= TX_PAYLOAD;
                                
                            when TX_PAYLOAD =>
                                if CAL_ENA_I = '1' and (FAST_BEAT_I = '1' or SLOW_BEAT_I  = '1') then
                                    frame_ctrl_fsm_state <= TX_PREAMBLE;
                                else
                                    frame_ctrl_fsm_state <= TX_GAP;
                                end if;
                                
                            when TX_GAP =>
                                frame_ctrl_fsm_state <= TX_IDLE;
                            
                            when others =>
                                frame_ctrl_fsm_state <= TX_IDLE;
                        end case;
                        
                end if;
            end process;

   --====================================================================================================--
   --============================= FRAME AUXILARY CONTROL USING SLOW BEAT ===============================--
   --====================================================================================================--

   FRAME_CTRL_AUXILARY_proc : 
   process (CLK_I) is
    begin
        if RESET_I = '1' then
            tx_k28_1 <= '0';
        elsif rising_edge(CLK_I) then
                if SLOW_BEAT_I = '1' and CAL_ENA_I = '1' then
                    tx_k28_1 <= '1';
                elsif frame_ctrl_fsm_state = TX_START_OF_FRAME then
                    tx_k28_1 <= '0';
                end if;
        end if;
    end process;

   --====================================================================================================--
   --======================================= FRAME GENERATOR ============================================--
   --====================================================================================================--
    
    FRAME_GEN_proc: 
    process (CLK_I)
    begin  
        if rising_edge(CLK_I) then
        
            case frame_ctrl_fsm_state is
                
                when TX_IDLE =>
                
                    TXDATA_O                     <= D24_5 & D24_5 & D24_5 & D24_5;  -- D24.3 to avoid DC unbalance
                    
                    TX8B10BBYPASS_O              <= "0000";
                    TXCHARISK_O                  <= "0000";
                
                when TX_PREAMBLE =>
                
                    TXDATA_O                     <= D21_5 & D21_5 & D21_5 & D21_5;  
                    
                    TX8B10BBYPASS_O              <= "0000";
                    TXCHARISK_O                  <= "0000";
                
                when TX_START_OF_FRAME =>
                
                    TXDATA_O (31 DOWNTO 16)     <= x"0000";
                    TXDATA_O (15 DOWNTO  8)     <= ADDRESS_I;
                    if tx_k28_1 = '1' then
                        TXDATA_O (7 DOWNTO 0)   <= K28_1;
                    else
                        TXDATA_O (7 DOWNTO 0)   <= K28_5;
                    end if;
                
                    TX8B10BBYPASS_O             <= "0000";
                    TXCHARISK_O                 <= "0001";
                    
                when TX_PAYLOAD =>
                
                    TXDATA_O                    <= TXDATA_I;
                    
                    TX8B10BBYPASS_O             <= "0000";
                    TXCHARISK_O                 <= "0000";
                
                when TX_GAP =>
                
                    TXDATA_O                    <= x"0000_0000";
                    
                    TX8B10BBYPASS_O             <= "1111";
                    TXCHARISK_O                 <= "0000";
                
                when others =>
                
                    TXDATA_O                    <= D24_3 & D24_3 & D24_3 & D24_3;  -- D24.3 to avoid DC unbalance
                    
                    TX8B10BBYPASS_O             <= "0000";
                    TXCHARISK_O                 <= "0000";
            end case;
        end if;
    end process;

end architecture mixed;
