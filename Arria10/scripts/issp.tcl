# Title      : PON RX PMA Clock Slip Controller
# Project    : TTC-PON 2015
# -----------------------------------------------------------------------------
# File       : 
# Author     : 
# Company    : 
# Created    : 19-03-2016
# Last update: 19-03-2016
# -----------------------------------------------------------------------------
# Description: 
# 
# BCC5BCC5 (32-bit 8B) = 0x5f1a5a0da5 (40-bit 10B)
# 0101111100 0110100101 1010000011 0110100101 (40-bit 10B)
# 0x17c (10 bit symbol)
# 
# -----------------------------------------------------------------------------
# Revisions  :
# Date        Version    Author        Description
# 19-03-2016  1.0      Erno David     	Created
#

# Functions

proc issp_read {} {
  global source_width
  global probe_width
  set issp [lindex [get_service_paths issp] 0]
  set claimed_issp [claim_service issp $issp mylib]
  array set instance_info [issp_get_instance_info $claimed_issp]
  set source_width $instance_info(source_width)
  set probe_width $instance_info(probe_width)
  set probe_data [issp_read_probe_data $claimed_issp]
  close_service issp $claimed_issp;
  return $probe_data
}

proc issp_write { value } {
  set issp [lindex [get_service_paths issp] 0]
  set claimed_issp [claim_service issp $issp mylib]
  issp_write_source_data $claimed_issp $value
  close_service issp $claimed_issp;
}

proc pon_reset {} {
  # reset
  puts "RESET"
  issp_write 0xc1
  after 200
  issp_write 0xc0
  after 200

  # initial dump
  set all_probe_data [issp_read]
  # puts "ISSP: $issp"
  # puts "source_width: $source_width"
  # puts "probe_width: $probe_width"
  puts "all_probe_data: $all_probe_data"
  set rx_data [expr ($all_probe_data >> 8) & 0xffffffffff]
  set rx_data_locked [expr $rx_data == 0x5f1a5a0da5]
  set lockedtodata [expr ($all_probe_data >> 2) & 1]
  set tx_clk_freq [expr ($all_probe_data >> 48) & 0xffffffff]
  set rx_clk_freq [expr ($all_probe_data >> 80) & 0xffffffff]
  # puts "rx_data: $rx_data ($rx_data_locked)"
  puts "lockedtodata: $lockedtodata"
  puts "tx_clk_freq: $tx_clk_freq"
  puts "rx_clk_freq: $rx_clk_freq"

  set rx_data [expr ($rx_data << 40) | $rx_data]
  set comapos -1
  for {set j 0} {$j < 40} {incr j} {
    if {[expr (($rx_data >> $j) & 0x3ff) == 0x17c]} {
      set comapos $j
      break
    }
  }

  puts "Coma found at: $comapos"
  return $comapos
}

proc pon_clkslip {} {
  # clkslip
  puts -nonewline "CLKSLIP:"
  for {set j 0} {$j <= 44} {incr j 1} {
    puts -nonewline " $j"
    issp_write 0xd0; issp_write 0xc0
    # after 10
    set all_probe_data [issp_read]
    set rx_data [expr ($all_probe_data >> 8) & 0xffffffffff]
    set rx_data_locked [expr $rx_data == 0x5f1a5a0da5]
    # puts -nonewline " $rx_data_locked"
    if {$rx_data_locked} {
        puts -nonewline " <- LOCKED"
      break
    }
  }
  puts ""
}

# Main

puts "================================================================"

# for {set i 0} {$i <= 3} {incr i} {
while 1 {
  # puts "Iteration: $i"
  set comapos [pon_reset]
  if {[expr ($comapos & 1) == 0]} {
    pon_clkslip
    # issp_write 0xd0; issp_write 0xc0
    # issp_write 0xd0; issp_write 0xc0
    # break
    after 3000
  }
}

puts ""
