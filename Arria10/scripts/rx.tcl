# Title      : REFCLK1 vs RX_PMA_WORD_CLK Phase Scanner
# Project    : TTC-PON 2015
# -----------------------------------------------------------------------------
# File       : 
# Author     : 
# Company    : 
# Created    : 20-03-2016
# Last update: 20-03-2016
# -----------------------------------------------------------------------------
# Description: 
# 
# -----------------------------------------------------------------------------
# Revisions  :
# Date        Version  Author  Description
# 20-03-2016  1.0      ed     	Created
#

if {[info exist claimed_issp] == 0} {
  set issp [lindex [get_service_paths issp] 0]
  set claimed_issp [claim_service issp $issp mylib]
}  

array set instance_info [issp_get_instance_info $claimed_issp]
set source_width $instance_info(source_width)
set probe_width $instance_info(probe_width)
set probe_data [issp_read_probe_data $claimed_issp]
set source_data [issp_read_source_data $claimed_issp]
puts "ISSP: $issp"
puts "source_width: $source_width"
puts "probe_width: $probe_width"
puts "probe_data: $probe_data"
puts "source_data: $source_data"

set MUX 3
set LOG "rx_tcl.csv"

for {set i 100} {$i < 200} {incr i} {
  # toggle rx_pma_clkslip_i
  issp_write_source_data $claimed_issp [expr ($MUX << 6) | (1 << 4)]
  after 10
  issp_write_source_data $claimed_issp [expr ($MUX << 6)]
  after 10

  # toggle PHASE_COMP_RESET_I 
  issp_write_source_data $claimed_issp [expr ($MUX << 6) | (1 << 9)]
  after 10
  issp_write_source_data $claimed_issp [expr ($MUX << 6)]
  after 10
  
  # wait for PHASE_CALC_DONE
  # probes(145) <= PHASE_CALC_DONE;
  while 1 {
    set probe_data [issp_read_probe_data $claimed_issp]
    set phase_calc_done [expr ($probe_data >> 145) & 1]
    if {$phase_calc_done} break
    after 10
  }
  
  # read phase values 
  # probes(144) <= PHASE_VALUE_SIGN;
  # probes(143 downto 112) <= PHASE_VALUE_INTEGER_PART;
  set probe_data [issp_read_probe_data $claimed_issp]
  set phase_value_sign [expr ($probe_data >> 144) & 1]
  set phase_value_integer_part [expr ($probe_data >> 112) & 0xffffffff]
  # puts "phase_value_sign: $phase_value_sign"
  # puts "phase_value_integer_part: $phase_value_integer_part"
  set phase_value [expr 1.0 * $phase_value_integer_part]
  if {$phase_value_sign} {
    set phase_value [expr -1.0 * $phase_value]
  }
  puts "$i : $probe_data : $phase_value"
  # append the results to the log file
  set fo [open $LOG a]
  puts $fo "$i, $probe_data, $phase_value"
  close $fo
}

puts ""
