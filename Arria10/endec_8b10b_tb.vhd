-------------------------------------------------------------------------------
--
-- Title	: Test Bench for enc_8b10b and dec_8b10b
-- Design	: 8b-10b Encoder/Decoder Test Bench
-- Project	: 8000 - 8b10b_encdec
-- Author	: Ken Boyette
-- Company	: Critia Computer, Inc.
--
-------------------------------------------------------------------------------
--
-- File			: encdec_8b10b_TB.vhd
-- Version		: 1.0
-- Generated	: 09.25.2006
-- From			: y:\Projects\8000\FPGA\VHDLSource\8b10b\8b10_enc.vhd
-- By			: Active-HDL Built-in Test Bench Generator ver. 1.2s
--
-------------------------------------------------------------------------------
--
-- Description : Test Bench for combined enc_8b10b_tb & dec_8b10b
--
--
--	This testbench provides a sequence of data pattern stimuli for the
--	enc_8b10b component.  It latches the encoded output and provides this
--	as input to the dec_8b10b component.  The test pattern generator
--	alternately drives all data patterns and then the 12 defined K patterns.
--	
-------------------------------------------------------------------------------
-- This program is licensed under the GPL
-- Revisions  :
-- Date        Version  Author          Description
-- 16-03-2016  1.0      Jubin Mitra	    Modified for TTC-PON Application
-------------------------------------------------------------------------------
 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
 
use work.CHARACTER_8b_10b_PACKAGE.all;

 
entity endec_8b10b_tb is
end endec_8b10b_tb;
 
architecture TB_ARCHITECTURE of endec_8b10b_tb is

--=================================================================================================--
						--========####  Component Declaration  ####========-- 
--=================================================================================================--   
 
	component enc_8b10b
	port(
		RESET : in std_logic;
		SBYTECLK : in std_logic;
		KI : in std_logic;
		AI : in std_logic;
		BI : in std_logic;
		CI : in std_logic;
		DI : in std_logic;
		EI : in std_logic;
		FI : in std_logic;
		GI : in std_logic;
		HI : in std_logic;
		AO : out std_logic;
		BO : out std_logic;
		CO : out std_logic;
		DO : out std_logic;
		EO : out std_logic;
		IO : out std_logic;
		FO : out std_logic;
		GO : out std_logic;
		HO : out std_logic;
		JO : out std_logic
		);
	end component;
 
 	component dec_8b10b
	port(
		RESET : in std_logic;
		RBYTECLK : in std_logic;
		AI : in std_logic;
		BI : in std_logic;
		CI : in std_logic;
		DI : in std_logic;
		EI : in std_logic;
		II : in std_logic;
		FI : in std_logic;
		GI : in std_logic;
		HI : in std_logic;
		JI : in std_logic;
		AO : out std_logic;
		BO : out std_logic;
		CO : out std_logic;
		DO : out std_logic;
		EO : out std_logic;
		FO : out std_logic;
		GO : out std_logic;
		HO : out std_logic;
		KO : out std_logic
		);
	end component;

    --=================================================================================================--
						--========####  Signal Declaration  ####========-- 
    --=================================================================================================--   
   
    constant refclk_period	 			   			  : time := 4.166   ns;			
    constant wait_period	 			   			  : time := 10      ns;			

    signal REF_CLK                                    : std_logic := '0';
    signal RESET                                      : std_logic := '0';
    
    signal data_counter                               : std_logic_vector(1 downto 0) := (others=>'0');
    
    signal tx_data                                    : std_logic_vector(7 downto 0) := (others=>'0');
    signal tx_is_control_char                         : std_logic                    := '0' ;
    signal enc_data                                   : std_logic_vector(9 downto 0) := (others=>'0');
    signal enc_data_l1                                : std_logic_vector(9 downto 0) := (others=>'0');
    signal dec_data                                   : std_logic_vector(7 downto 0) := (others=>'0');
    signal dec_is_control_char                        : std_logic                    := '0' ;

begin
	---------------------------------------------------------------------------
	-- Instantiate modules
	---------------------------------------------------------------------------
	encoder : enc_8b10b 
		port map (
			RESET => RESET,
			SBYTECLK => REF_CLK,
			KI => tx_is_control_char,
			AI => tx_data(0),
			BI => tx_data(1),
			CI => tx_data(2),
			DI => tx_data(3),
			EI => tx_data(4),
			FI => tx_data(5),
			GI => tx_data(6),
			HI => tx_data(7),
			AO => enc_data(0),
			BO => enc_data(1),
			CO => enc_data(2),
			DO => enc_data(3),
			EO => enc_data(4),
			IO => enc_data(5),
			FO => enc_data(6),
			GO => enc_data(7),
			HO => enc_data(8),
			JO => enc_data(9)
		);
	decoder : dec_8b10b 
		port map (
			RESET => RESET,
			RBYTECLK => REF_CLK,
			AI => enc_data_l1(0),	-- Note: Use the latched encoded data
			BI => enc_data_l1(1),
			CI => enc_data_l1(2),
			DI => enc_data_l1(3),
			EI => enc_data_l1(4),
			II => enc_data_l1(5),
			FI => enc_data_l1(6),
			GI => enc_data_l1(7),
			HI => enc_data_l1(8),
			JI => enc_data_l1(9),
			AO => dec_data(0),
			BO => dec_data(1),
			CO => dec_data(2),
			DO => dec_data(3),
			EO => dec_data(4),
			FO => dec_data(5),
			GO => dec_data(6),
			HO => dec_data(7),
			KO => dec_is_control_char
		);
 
   --==================================== Clock Generation =====================================--

   refclk_gen: 
   process
   begin
		
		REF_CLK <= '1';
		wait for refclk_period/2;
		REF_CLK <= '0';
		wait for refclk_period/2;
   end process;
   
    --==================================== User Logic =====================================--

    data_gen_proc:
    process(RESET,REF_CLK)
    begin
        if RESET = '1' then
                tx_data             <= (others=>'0');
                tx_is_control_char  <= '1';
                data_counter        <= "00";
        elsif rising_edge(REF_CLK) then
                enc_data_l1         <= enc_data;
                data_counter        <= data_counter + '1';
                
            if    data_counter="00" then
                tx_data             <= D24_5;
                tx_is_control_char  <= '0';
            elsif data_counter="01" then
                tx_data             <= D21_5;
                tx_is_control_char  <= '0';
            elsif data_counter="10" then
                tx_data             <= K28_5;
                tx_is_control_char  <= '1'; 
            elsif data_counter="11" then
                tx_data             <= D24_3;
                tx_is_control_char  <= '0';
            end if;
            
        end if;
    end process;
    
	test_proc:
	process
	begin
		RESET <= '1';
		wait for wait_period;
		reset <= '0';
		wait for wait_period*10;
		wait;

	end process;
		
   --=====================================================================================--     
end TB_ARCHITECTURE;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================-- 