----------------------------------------------------------------------------------
-- Company: CERN ESE-BE
-- Engineer: Kolotouros Dimitrios-Marios
-- Code provided by Paschalis Vichoudis
-- 
-- Create Date:    10:13:18 01/29/2013 
-- Design Name: 
-- Module Name:    new_barrel - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------



------------------------------------------------------
-- Right_Shifter_39b                               --
--                                                 --
-- Shifts data from the left to right              --
--                                                 --
-- Author: Fr�d�ric Marin                          --
-- Date: September 25th, 2008                      --
------------------------------------------------------
-- 																--
-- Converted to Left_Shifter April 24th, 2013		--
--																	--
------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
                                        
	ENTITY new_barrel IS
		PORT(
			clock_i                : IN    STD_LOGIC;
			din_i                  : IN    STD_LOGIC_VECTOR(39 DOWNTO 0);
			nshift_i               : IN    STD_LOGIC_VECTOR(5 DOWNTO 0);
			dout_o            		: OUT   STD_LOGIC_VECTOR(39 DOWNTO 0));  

	END new_barrel;


ARCHITECTURE a OF new_barrel IS

SIGNAL Previous_Word : STD_LOGIC_VECTOR(39 DOWNTO 0);

        BEGIN
        
        PROCESS (clock_i)
        
                BEGIN
                
                IF RISING_EDGE(clock_i) THEN
                        Previous_Word              <= din_i;
                        CASE nshift_i IS
                                WHEN "000000" => -- 0
                                        dout_o <= din_i;
                                WHEN "000001" => -- 1
                                        dout_o <= din_i(38 DOWNTO 0) & Previous_Word(39 DOWNTO 39);
                                WHEN "000010" => -- 2
                                        dout_o <= din_i(37 DOWNTO 0) & Previous_Word(39 DOWNTO 38);
                                WHEN "000011" => -- 3
                                        dout_o <= din_i(36 DOWNTO 0) & Previous_Word(39 DOWNTO 37);
                                WHEN "000100" => -- 4
                                        dout_o <= din_i(35 DOWNTO 0) & Previous_Word(39 DOWNTO 36);
                                WHEN "000101" => -- 5
                                        dout_o <= din_i(34 DOWNTO 0) & Previous_Word(39 DOWNTO 35);
                                WHEN "000110" => -- 6
                                        dout_o <= din_i(33 DOWNTO 0) & Previous_Word(39 DOWNTO 34);
                                WHEN "000111" => -- 7
                                        dout_o <= din_i(32 DOWNTO 0) & Previous_Word(39 DOWNTO 33);
                                WHEN "001000" => -- 8
                                        dout_o <= din_i(31 DOWNTO 0) & Previous_Word(39 DOWNTO 32);
                                WHEN "001001" => -- 9
                                        dout_o <= din_i(30 DOWNTO 0) & Previous_Word(39 DOWNTO 31);
                                WHEN "001010" => -- 10
                                        dout_o <= din_i(29 DOWNTO 0) & Previous_Word(39 DOWNTO 30);
                                WHEN "001011" => -- 11
                                        dout_o <= din_i(28 DOWNTO 0) & Previous_Word(39 DOWNTO 29);
                                WHEN "001100" => -- 12
                                        dout_o <= din_i(27 DOWNTO 0) & Previous_Word(39 DOWNTO 28);
                                WHEN "001101" => -- 13
                                        dout_o <= din_i(26 DOWNTO 0) & Previous_Word(39 DOWNTO 27);
                                WHEN "001110" => -- 14
                                        dout_o <= din_i(25 DOWNTO 0) & Previous_Word(39 DOWNTO 26);
                                WHEN "001111" => -- 15
                                        dout_o <= din_i(24 DOWNTO 0) & Previous_Word(39 DOWNTO 25);
                                WHEN "010000" => -- 16
                                        dout_o <= din_i(23 DOWNTO 0) & Previous_Word(39 DOWNTO 24);
                                WHEN "010001" => -- 17
                                        dout_o <= din_i(22 DOWNTO 0) & Previous_Word(39 DOWNTO 23);
                                WHEN "010010" => -- 18
                                        dout_o <= din_i(21 DOWNTO 0) & Previous_Word(39 DOWNTO 22);
                                WHEN "010011"  => -- 19
                                        dout_o <= din_i(20 DOWNTO 0) & Previous_Word(39 DOWNTO 21);
													 
										  WHEN "010100" => -- 20
                                        dout_o <= din_i(19 DOWNTO 0) & Previous_Word(39 DOWNTO 20);		 
                                WHEN "010101" => -- 21
                                        dout_o <= din_i(18 DOWNTO 0) & Previous_Word(39 DOWNTO 19);
                                WHEN "010110" => -- 22
                                        dout_o <= din_i(17 DOWNTO 0) & Previous_Word(39 DOWNTO 18);
                                WHEN "010111" => -- 23
                                        dout_o <= din_i(16 DOWNTO 0) & Previous_Word(39 DOWNTO 17);
                                WHEN "011000" => -- 24
                                        dout_o <= din_i(15 DOWNTO 0) & Previous_Word(39 DOWNTO 16);
                                WHEN "011001" => -- 25
                                        dout_o <= din_i(14 DOWNTO 0) & Previous_Word(39 DOWNTO 15);
                                WHEN "011010" => -- 26
                                        dout_o <= din_i(13 DOWNTO 0) & Previous_Word(39 DOWNTO 14);
                                WHEN "011011" => -- 27
                                        dout_o <= din_i(12 DOWNTO 0) & Previous_Word(39 DOWNTO 13);
                                WHEN "011100" => -- 28
                                        dout_o <= din_i(11 DOWNTO 0) & Previous_Word(39 DOWNTO 12);
                                WHEN "011101" => -- 29
                                        dout_o <= din_i(10 DOWNTO 0) & Previous_Word(39 DOWNTO 11);
                                WHEN "011110" => -- 30
                                        dout_o <= din_i(9 DOWNTO 0) & Previous_Word(39 DOWNTO 10);
                                WHEN "011111" => -- 31
                                        dout_o <= din_i(8 DOWNTO 0) & Previous_Word(39 DOWNTO 9);
                                WHEN "100000" => -- 32
                                        dout_o <= din_i(7 DOWNTO 0) & Previous_Word(39 DOWNTO 8);
                                WHEN "100001" => -- 33
                                        dout_o <= din_i(6 DOWNTO 0) & Previous_Word(39 DOWNTO 7);
                                WHEN "100010" => -- 34
                                        dout_o <= din_i(5 DOWNTO 0) & Previous_Word(39 DOWNTO 6);
                                WHEN "100011" => -- 35
                                        dout_o <= din_i(4 DOWNTO 0) & Previous_Word(39 DOWNTO 5);
                                WHEN "100100" => -- 36
                                        dout_o <= din_i(3 DOWNTO 0) & Previous_Word(39 DOWNTO 4);
                                WHEN "100101" => -- 37
                                        dout_o <= din_i(2 DOWNTO 0) & Previous_Word(39 DOWNTO 3);
                                WHEN "100110" => -- 38
                                        dout_o <= din_i(1 DOWNTO 0) & Previous_Word(39 DOWNTO 2);
                                WHEN "100111" => -- 39
                                        dout_o <= din_i(0 DOWNTO 0) & Previous_Word(39 DOWNTO 1);			 
										  WHEN others  => -- 40
                                        dout_o <= Previous_Word;			 

                        END CASE;
                END IF;
        END PROCESS;

END a;