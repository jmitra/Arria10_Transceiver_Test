-------------------------------------------------------------------------------
-- Title      : Frequency Calculator
-- Project    : Calculation of unknown frequency with respect to known frequency 
-------------------------------------------------------------------------------
-- File       : FrequencyCalculator.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 15-02-2015
-- Last update: 15-02-2015
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- General : It has an accuracy of 2 digit after decimal, however it displays upto 4 decimal places. (Value Format : XXX.XX00)
-- Following parameter must be set:
-- Generic parameter: REFERENCE_FREQUENCY_VALUE in integer (MHz)
-- Other parameters:
-- To control the speed of computation and accuracy of phase measurement
-- Internal Signal: ratio_precision (32 bit value) = Default :  x"1000_0000“
-- To control the precision of division in phase computation
-- Internal Signal: ratio_precision_decimal_places (32 bit values) = Default : x"0000_68DB“

-- ratio_precision_decimal_places = ratio_precision / 10000 (in decimal)

-- GENERIC : Reference Frequency Value
-- INPUT   : Reference Clock
--			 Unknown   Clock
-- OUTPUT  : Calculated Frequency Value								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 15-02-2015  1.0      Jubin	Created
-- 19-02-2015  1.0      Jubin	Put unknown_clk_counter_stop signal to prevent CDC related error
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;
use IEEE.math_real.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity FrequencyCalculator is
   generic (
		constant REFERENCE_FREQUENCY_VALUE    			: integer:= 100
   );
   port (
  
      --================--
      -- Reset & Clocks --
      --================--    
      
      -- Reset:
      ---------
      
      RESET_I                                    : in  std_logic;
  
      -- REFERENCE Clock:
      ---------------
      
      CLK_REF_I   		                         : in  std_logic; 
      
	  -- UNKNOWN Clock:
      ---------------
      
      CLK_UNKNOWN_I   		                     : in  std_logic; 
	  
      -- Output Frequency Value:
      ------------------------
      
      CLK_FREQUENCY_VALUE_O						 : out std_logic_vector(31 downto 0);
      COMPUTATION_DONE_O                         : out std_logic
   
   );
end entity;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--
architecture behavioral of FrequencyCalculator is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   
	signal s_REFERENCE_FREQUENCY_VALUE  		: std_logic_vector(31 downto 0):=(others=>'0');
	signal ref_clk_counter						: std_logic_vector(31 downto 0):=(others=>'0');
	signal unknown_clk_counter					: std_logic_vector(31 downto 0):=(others=>'0');
	signal ratio_divisor                   		: std_logic_vector(63 downto 0):=(others=>'0');
	signal ratio_precision                 		: std_logic_vector(31 downto 0):=x"1000_0000";
	signal ratio_precision_decimal_places  		: std_logic_vector(31 downto 0):=x"0000_68DB"; -- ratio_precision /1000
    signal ratio_multiplier                     : std_logic_vector(63 downto 0):=(others=>'0');
    signal ratio_trace_counter                  : std_logic_vector(63 downto 0):=(others=>'0');
	signal unknown_freq_value             		: std_logic_vector(31 downto 0):=(others=>'0');
    
    signal start_division                       : std_logic                    := '0';
    signal reset                                : std_logic                    := '0';
    signal soft_reset                           : std_logic                    := '0';
    signal computation_done                     : std_logic                    := '0';
	signal unknown_clk_counter_stop   			: std_logic                    := '0';
	
	--==================================== Component Declaration ====================================--          

   
   --==============================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== User Logic ==============================================--   
    reset                       <= RESET_I or soft_reset;
   
   --===================================================================================================--
   --================================# Reference Clock Driven Counter #=================================--
   --===================================================================================================--
  ref_counter_proc:
  process(reset,CLK_REF_I)
	begin
		if reset = '1' then
			ref_clk_counter		<= (others=>'0');
		elsif rising_edge(CLK_REF_I) then
			ref_clk_counter		<= ref_clk_counter + '1';
		end if;
	end process;
	
   --===================================================================================================--
   --================================# Unknown Clock Driven Counter #=================================--
   --===================================================================================================--
  unknown_counter_proc:
  process(reset,CLK_UNKNOWN_I)
	begin
		if reset = '1' then
			unknown_clk_counter		<= (others=>'0');
		elsif rising_edge(CLK_UNKNOWN_I) then
            if unknown_clk_counter_stop = '0' then
                unknown_clk_counter		<= unknown_clk_counter + '1';   
            end if;
		end if;            
	end process;
	
    
    
   --===================================================================================================--
   --================================# Ratio Divisor Finder #===============--
   --===================================================================================================--
      
  ratio_finder:
  process(reset,CLK_REF_I)
	begin
		if reset = '1' then
				ratio_multiplier	        <= (others=>'0');
                start_division              <= '0';
                unknown_clk_counter_stop    <= '0';
		elsif rising_edge(CLK_REF_I) then
			if ref_clk_counter = ratio_precision and start_division = '0' then
                    unknown_clk_counter_stop <= '1';
			end if;
            if unknown_clk_counter_stop = '1' then
                    ratio_multiplier	<=  x"0000_0000" & unknown_clk_counter;
                    start_division      <= '1';
            end if;
		end if;
	end process;	
	
    s_REFERENCE_FREQUENCY_VALUE      <= std_logic_vector(to_unsigned(REFERENCE_FREQUENCY_VALUE,32));
    ratio_divisor                    <= x"0000_0000" & ratio_precision_decimal_places;
   
   
   --===================================================================================================--
   --================================#  Freq Value Calculator #=====================================--
   --===================================================================================================--	
    buffer_results:
    process(reset,CLK_REF_I)
    begin
        if reset='1' then
            unknown_freq_value    <= (others=>'0');
            ratio_trace_counter   <= (others=>'0');
            computation_done      <= '0';
            soft_reset            <= '0';
        elsif rising_edge(CLK_REF_I) then
            
            if ratio_trace_counter < ratio_multiplier and start_division = '1' then
                unknown_freq_value      <= unknown_freq_value  + s_REFERENCE_FREQUENCY_VALUE; -- Does unit division if adder is '1' else multiplication simultanously with s_REFERENCE_FREQUENCY_VALUE
                ratio_trace_counter     <= ratio_trace_counter + ratio_divisor;
            elsif ratio_trace_counter >= ratio_multiplier and start_division = '1' then
                CLK_FREQUENCY_VALUE_O       <= unknown_freq_value - s_REFERENCE_FREQUENCY_VALUE;
                computation_done            <= '1';
            end if;
            
            if computation_done = '1' then
                 soft_reset                  <= '1';
            end if;
        end if;
    end process;
    COMPUTATION_DONE_O                       <= computation_done;
	
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--