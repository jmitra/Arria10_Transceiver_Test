--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
-------------------------------------------------------------------------------
-- Title      : PHYSICAL CODING SUBLAYER CONNECTIONS 
-- Project    : TTC-PON 2015
-------------------------------------------------------------------------------
-- File       : TTC_PCS_LAYER.vhd
-- Author     : Jubin MITRA (jmitra@cern.ch)
-- Company    : VECC
-- Created    : 16-03-2016
-- Last update: 16-03-2016
-- Platform   : 
-- Standard   : VHDL'93/08                  
-- Revision   : 1.0                                                     
-- Target Device:         Altera Arria 10                                                          
-- Tool version:          Quartus II 15.1                                                                
-------------------------------------------------------------------------------
-- Description: 
-- It connects all the modules related to Physical Coding Sublayer								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 16-03-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.TTC_PON_PACKAGE.all;
--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity TTC_PCS_LAYER is 
port (
-- RESET
        GENERAL_RESET_I                      : in std_logic;
        TX_PCS_RESET_I                       : in std_logic;
        RX_PCS_RESET_I                       : in std_logic;
        
-- CLOCKS
        TX_PMA_WORD_CLK_I                    : in std_logic;
        RX_PMA_WORD_CLK_I                    : in std_logic;

-- BUS LINES
        TTC_PCS_I                            : in  pcs_signals_i;
        TTC_PCS_O                            : out pcs_signals_o
        
);
end entity TTC_PCS_LAYER;


--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture mixed of TTC_PCS_LAYER is

	--====================================== Constant Definition ====================================--   
	

	--====================================== Signal Definition ======================================--   
    
    signal TX_RESET                                         : std_logic;
    signal RX_RESET                                         : std_logic;
    
    --8b10 Encoder Decoder
    signal enc_8b10b_data_i                                 : std_logic_vector(31 downto 0);
    signal enc_8b10b_data_o                                 : std_logic_vector(39 downto 0);
    signal enc_8b10b_is_control_char_i                      : std_logic_vector( 3 downto 0);
    signal dec_8b10b_data_i                                 : std_logic_vector(39 downto 0);
    signal dec_8b10b_data_o                                 : std_logic_vector(31 downto 0);
    signal dec_8b10b_is_control_char_o                      : std_logic_vector( 3 downto 0);
     
    -- ONU FRAME GENERATOR
    signal onu_frame_gen_cal_ena_i                          : std_logic;
    signal onu_frame_gen_fast_beat_i                        : std_logic;
    signal onu_frame_gen_slow_beat_i                        : std_logic;
    signal onu_frame_gen_frame_ena_i                        : std_logic;
    signal onu_frame_gen_tx_data_i                          : std_logic_vector(31 downto 0);
    signal onu_frame_gen_address_i                          : std_logic_vector( 7 downto 0);
    signal onu_frame_gen_ready_o                            : std_logic;
    signal onu_frame_gen_txdata_o                           : std_logic_vector(31 downto 0);
    signal onu_frame_gen_tx8b10bypass_o                     : std_logic_vector( 3 downto 0);
    signal onu_frame_gen_txcharisk_o                        : std_logic_vector( 3 downto 0);
    
    -- ONU WORD ALIGNER
    signal onu_word_aligner_enable_i                        : std_logic;
    signal onu_word_aligner_rxdata_i                        : std_logic_vector(39 downto 0);
    signal onu_word_aligner_nshift_o                        : std_logic_vector( 5 downto 0);
    signal onu_word_aligner_aligned_o                       : std_logic;
    signal onu_word_aligner_reset_gtx_o                     : std_logic;
    signal onu_word_aligner_rx_slide_o                      : std_logic;
    signal onu_word_aligner_rx8b10ben_o                     : std_logic;
    signal onu_word_aligner_comma_detect_o                  : std_logic;
    signal onu_word_aligner_rx_k28_5_o                      : std_logic;
    signal onu_word_aligner_rx_k28_1_o                      : std_logic;
    
    -- BARREL SHIFTER
    signal barrel_shift_din_i                               : std_logic_vector(39 downto 0);
    signal barrel_shift_nshift_i                            : std_logic_vector( 5 downto 0);
    signal barrel_shift_dout_o                              : std_logic_vector(39 downto 0);

    -- INTERNAL SIGNALS
    signal tx_parallel_data                                 : std_logic_vector(39 downto 0);

    --==================================== Component Declaration ====================================--          

    -- 8b10b Encoder Decoder
    component enc_8b10b
	port(
		RESET : in std_logic;
		SBYTECLK : in std_logic;
		KI : in std_logic;
		AI : in std_logic;
		BI : in std_logic;
		CI : in std_logic;
		DI : in std_logic;
		EI : in std_logic;
		FI : in std_logic;
		GI : in std_logic;
		HI : in std_logic;
		AO : out std_logic;
		BO : out std_logic;
		CO : out std_logic;
		DO : out std_logic;
		EO : out std_logic;
		IO : out std_logic;
		FO : out std_logic;
		GO : out std_logic;
		HO : out std_logic;
		JO : out std_logic
		);
	end component;
 
 	component dec_8b10b
	port(
		RESET : in std_logic;
		RBYTECLK : in std_logic;
		AI : in std_logic;
		BI : in std_logic;
		CI : in std_logic;
		DI : in std_logic;
		EI : in std_logic;
		II : in std_logic;
		FI : in std_logic;
		GI : in std_logic;
		HI : in std_logic;
		JI : in std_logic;
		AO : out std_logic;
		BO : out std_logic;
		CO : out std_logic;
		DO : out std_logic;
		EO : out std_logic;
		FO : out std_logic;
		GO : out std_logic;
		HO : out std_logic;
		KO : out std_logic
		);
	end component;
    
    -- FRAME GENERATOR
    component ONU_FRAME_GEN 
    port (
        CLK_I             : in  std_logic;  
        RESET_I           : in  std_logic;  

        -- CALIBRATION ENABLE SIGNALS
        CAL_ENA_I         : in  std_logic;   
        FAST_BEAT_I       : in  std_logic;
        SLOW_BEAT_I       : in  std_logic;
        
        -- STANDARD FRAME TX ENABLE SIGNAL
        FRAME_ENA_I       : in  std_logic;
        
        -- INPUT DATA WITH ONU ADDRESS
        TXDATA_I          : in  std_logic_vector(31 downto 0);
        ADDRESS_I         : in  std_logic_vector(7 downto 0);
        READY_O           : out std_logic;
        
        -- 8b10b ENCODED DATA
        TXDATA_O          : out std_logic_vector(31 downto 0);
        TX8B10BBYPASS_O   : out std_logic_vector(3 downto 0);
        TXCHARISK_O       : out std_logic_vector(3 downto 0)
        
        );
    end component;
         
    -- WORD ALIGNER
    component onu_word_align
    generic (
        g_COMMAMASK     : std_logic_vector := "0001111111";
        g_PCOMMAPATTERN : std_logic_vector := "0101111100";
        g_NCOMMAPATTERN : std_logic_vector := "1010000011");
    port (
        clock_i        : in  std_logic;
        reset_i        : in  std_logic;
        enable_i       : in  std_logic;
        rxdata_i       : in  std_logic_vector(39 downto 0);
        nshift_o       : out std_logic_vector(5 downto 0);
        aligned_o      : out std_logic;
        reset_gtx_o    : out std_logic;
        rx_slide_o     : out std_logic;
        rx8b10ben_o    : out std_logic;
        comma_detect_o : out std_logic;
        rx_k28_5_o     : out std_logic;
        rx_k28_1_o     : out std_logic
        );
    end component;

    -- BARREL SHIFTER
    component new_barrel 
		PORT(
			clock_i                : in    STD_LOGIC;
			din_i                  : in    STD_LOGIC_VECTOR(39 DOWNTO 0);
			nshift_i               : in    STD_LOGIC_VECTOR(5 DOWNTO 0);
			dout_o            	   : out   STD_LOGIC_VECTOR(39 DOWNTO 0)
        );  
	end component;


 
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
 
   --==================================== Port Mapping ==============================================--  
    encoder_decoder_8b10_genx4:
    for i in 0 to 3 generate
	encoder_8b10b_comp : 
    enc_8b10b 
		port map (
			RESET => TX_RESET,
			SBYTECLK => TX_PMA_WORD_CLK_I,
			KI => enc_8b10b_is_control_char_i(i),
			AI => enc_8b10b_data_i(i*8+0),
			BI => enc_8b10b_data_i(i*8+1),
			CI => enc_8b10b_data_i(i*8+2),
			DI => enc_8b10b_data_i(i*8+3),
			EI => enc_8b10b_data_i(i*8+4),
			FI => enc_8b10b_data_i(i*8+5),
			GI => enc_8b10b_data_i(i*8+6),
			HI => enc_8b10b_data_i(i*8+7),
			AO => enc_8b10b_data_o(i*10+0),
			BO => enc_8b10b_data_o(i*10+1),
			CO => enc_8b10b_data_o(i*10+2),
			DO => enc_8b10b_data_o(i*10+3),
			EO => enc_8b10b_data_o(i*10+4),
			IO => enc_8b10b_data_o(i*10+5),
			FO => enc_8b10b_data_o(i*10+6),
			GO => enc_8b10b_data_o(i*10+7),
			HO => enc_8b10b_data_o(i*10+8),
			JO => enc_8b10b_data_o(i*10+9)
		);

    decoder_8b10b_comp : 
    dec_8b10b 
		port map (
			RESET => RX_RESET,
			RBYTECLK => RX_PMA_WORD_CLK_I,
			AI => dec_8b10b_data_i(i*10+0),	-- Note: Use the latched encoded data
			BI => dec_8b10b_data_i(i*10+1),
			CI => dec_8b10b_data_i(i*10+2),
			DI => dec_8b10b_data_i(i*10+3),
			EI => dec_8b10b_data_i(i*10+4),
			II => dec_8b10b_data_i(i*10+5),
			FI => dec_8b10b_data_i(i*10+6),
			GI => dec_8b10b_data_i(i*10+7),
			HI => dec_8b10b_data_i(i*10+8),
			JI => dec_8b10b_data_i(i*10+9),
			AO => dec_8b10b_data_o(i*8+0),
			BO => dec_8b10b_data_o(i*8+1),
			CO => dec_8b10b_data_o(i*8+2),
			DO => dec_8b10b_data_o(i*8+3),
			EO => dec_8b10b_data_o(i*8+4),
			FO => dec_8b10b_data_o(i*8+5),
			GO => dec_8b10b_data_o(i*8+6),
			HO => dec_8b10b_data_o(i*8+7),
			KO => dec_8b10b_is_control_char_o(i)
		);
    end generate;
    
    onu_frame_gen_comp:
    ONU_FRAME_GEN 
        port map (
            CLK_I             => TX_PMA_WORD_CLK_I,
            RESET_I           => TX_RESET, 

            -- CALIBRATION ENABLE SIGNALS
            CAL_ENA_I         => onu_frame_gen_cal_ena_i,   
            FAST_BEAT_I       => onu_frame_gen_fast_beat_i,
            SLOW_BEAT_I       => onu_frame_gen_slow_beat_i,
            
            -- STANDARD FRAME TX ENABLE SIGNAL
            FRAME_ENA_I       => onu_frame_gen_frame_ena_i,
            
            -- INPUT DATA WITH ONU ADDRESS
            TXDATA_I          => onu_frame_gen_tx_data_i,
            ADDRESS_I         => onu_frame_gen_address_i,
            READY_O           => onu_frame_gen_ready_o,
            
            -- 8b10b ENCODED DATA
            TXDATA_O          => onu_frame_gen_txdata_o,
            TX8B10BBYPASS_O   => onu_frame_gen_tx8b10bypass_o,
            TXCHARISK_O       => onu_frame_gen_txcharisk_o
        
        );

    onu_word_align_comp:
    onu_word_align
    generic map(
        g_COMMAMASK           => "0001111111",
        g_PCOMMAPATTERN       => "0101111100",
        g_NCOMMAPATTERN       => "1010000011"
        )
    port map(
        clock_i               => RX_PMA_WORD_CLK_I,
        reset_i               => RX_RESET,
        enable_i              => onu_word_aligner_enable_i,
        rxdata_i              => onu_word_aligner_rxdata_i,
        nshift_o              => onu_word_aligner_nshift_o,
        aligned_o             => onu_word_aligner_aligned_o,
        reset_gtx_o           => onu_word_aligner_reset_gtx_o,
        rx_slide_o            => onu_word_aligner_rx_slide_o,
        rx8b10ben_o           => onu_word_aligner_rx8b10ben_o,
        comma_detect_o        => onu_word_aligner_comma_detect_o,
        rx_k28_5_o            => onu_word_aligner_rx_k28_5_o,
        rx_k28_1_o            => onu_word_aligner_rx_k28_1_o
        );

    barrel_shift_comp:
    new_barrel
    port map(
        clock_i                => RX_PMA_WORD_CLK_I,
		din_i                  => barrel_shift_din_i,
		nshift_i               => barrel_shift_nshift_i,
		dout_o            	   => barrel_shift_dout_o
    );  

   --==============================================================================================--
   --=================================== RESET CONNECTIONS ========================================--
   --==============================================================================================--
    TX_RESET                    <= GENERAL_RESET_I or TX_PCS_RESET_I;
    RX_RESET                    <= GENERAL_RESET_I or RX_PCS_RESET_I;

   --==============================================================================================--
   --===================== DATA AND CONTROL SIGNAL CONNECTIONS ====================================--
   --==============================================================================================--
   -- TRANSMISSION SIDE
   -- USER DATA PAYLOAD----> ONU FRAME GENERATOR ---> 8b10b ENCODER --->   XCVR PHY ---> ...........
   
   -- ONU FRAME GENERATOR
    onu_frame_gen_cal_ena_i                          <= TTC_PCS_I.cal_ena_i;
    onu_frame_gen_fast_beat_i                        <= TTC_PCS_I.fast_beat_i;
    onu_frame_gen_slow_beat_i                        <= TTC_PCS_I.slow_beat_i;
    onu_frame_gen_frame_ena_i                        <= TTC_PCS_I.frame_ena_i;
    onu_frame_gen_tx_data_i                          <= TTC_PCS_I.tx_data_payload_i;
    onu_frame_gen_address_i                          <= TTC_PCS_I.onu_address_i;
    TTC_PCS_O.tx_ready_o                             <= onu_frame_gen_ready_o;  
        
    --8b10 Encoder     
    enc_8b10b_data_i                                 <= onu_frame_gen_txdata_o;
    enc_8b10b_is_control_char_i                      <= onu_frame_gen_txcharisk_o;
    
    process(TX_PMA_WORD_CLK_I)
    begin
        if rising_edge(TX_PMA_WORD_CLK_I) then
            if onu_frame_gen_tx8b10bypass_o = "1111" then
                TTC_PCS_O.tx_parallel_data_o        <= (others=>'0');
            else
                TTC_PCS_O.tx_parallel_data_o        <= enc_8b10b_data_o;
            end if;
        end if;        
    end process;
    
   --========================================================================================================--
   -- RECEIVER SIDE
   -- ........... ---> XCVR PHY ---> BARREL SHIFTER ---> WORD ALIGNER ---> 8b10b DECODER ---> USER DATA PAYLOAD

    -- BARREL SHIFTER
    barrel_shift_din_i                               <= TTC_PCS_I.rx_parallel_data_i;
          
    -- ONU WORD ALIGNER
    onu_word_aligner_enable_i                        <= TTC_PCS_I.rx_phy_ready_i;
    onu_word_aligner_rxdata_i                        <= barrel_shift_dout_o;
    onu_word_aligner_nshift_o                        <= barrel_shift_nshift_i;
    TTC_PCS_O.aligned_o                              <= onu_word_aligner_aligned_o;
    TTC_PCS_O.reset_gtx_o                            <= onu_word_aligner_reset_gtx_o;
    TTC_PCS_O.rx_slide_o                             <= onu_word_aligner_rx_slide_o;
    TTC_PCS_O.rx8b10ben_o                            <= onu_word_aligner_rx8b10ben_o;
    TTC_PCS_O.comma_detect_o                         <= onu_word_aligner_comma_detect_o;
    TTC_PCS_O.rx_k28_5_o                             <= onu_word_aligner_rx_k28_5_o;
    TTC_PCS_O.rx_k28_1_o                             <= onu_word_aligner_rx_k28_1_o;
    
    -- 8b10b DECODER
    dec_8b10b_data_i                                 <= TTC_PCS_I.rx_parallel_data_i;
                                                        
    TTC_PCS_O.rx_data_payload_o                      <= dec_8b10b_data_o;
    TTC_PCS_O.is_control_char_o                      <= dec_8b10b_is_control_char_o;


end architecture mixed;